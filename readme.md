# Connect

Simple Java/Android library for working with HTTP.

## Examples

### Perform a GET request and get is ok status of the response

```java
Connect
	.get("http://stackoverflow.com/")
	.asChrome()
	.execute()
	.isOk()
```

### Save file from url

```java
long res = Connect.get("https://www.atlassian.com/wac/sectionWrap/08/column/00/moreContent/0/imageBinary/logo_jirasoftware-blue.svg")
	.withAuthorization(new BasicAuthorization("login", "password"));
	.execute()
	.save(new File("logo.svg"), true);
```


## API Requset Connect.[get/post/put/delete]

### Work with headers

```java
withHeader("Connection", "keep-alive");
withHeaders(new HashMap<String, String>() {{
	put("Connection", "keep-alive");
	put("Cache-Control", "max-age=0");
}});
range(0, 100);
rangeFrom(1024);
rangeTo(128);
```

### Work with cookie

```java
withCookie("ngx_uid", "sweqw2431231wKgJ2VY7NsNB6VNOE3X==");
withUserAgent("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36");
```

### Send headers like a Chrome

```java
asChrome();
```

### Work with query params

```java
addQuery("q", "query string");
```

### Work with forms params

```java
addDataForm("q", "query string");
addDataForms("q=query string&q2=1");
```

### Send any data 

```java
withData(new FileInputStream(new File("file1"));
withData(new StringBuilder("text");
```

### With Follow redirects

```java
withFollowRedirect(true)
```

### With authorization

```java
withAuthorization(new OAuthAuthorization("000access000token000"));
withAuthorization(new BasicAuthorization("login", "password"));
```

## API Response

### Get response code

```java
isOk();
isRederect()
getCode();
```

### Get response body

```java
asString();
asStringIfOk();
asJson();
asJsonIfOk();
save("file2", allowOverwrite, (bytes, len) -> {
	System.out.println("" + bytes + " / " + len);
});

```

### Work with response headers

```java
getHeader("Content-Type");
getContentType();
getContentRange();
getLastModified();
getExpires();
```

### Work with response set-cookie

```java
getSetCookie("ngx_uid");
```

## License

This library is available under the MIT License.

Copyright (c) 2016 Vadim Babin <vadim.babin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.