package org.vadel.libs.connect;

public class BasicAuthorization implements Authorization {

	final String token;
	
	public BasicAuthorization(String token) {
		this.token = token;
	}
	
	public BasicAuthorization(String login, String pass) {
		this.token = new String( Base64.encode((login + ":" + pass).getBytes()) );
	}
	
	@Override
	public void setCredentials(Request conn) {
		conn.headers.put("Authorization", "Basic " + token);
	}

}
