package org.vadel.libs.connect;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * Created by vadimbabin on 02.09.14.
 */
public class Connect {
	
	public static Request get(StringBuilder url) {
		return new Request(Request.METHOD_GET, url);
	}

	public static Request get(String url) {
		return new Request(Request.METHOD_GET, new StringBuilder(url));
	}

	public static Request head(StringBuilder url) {
		return new Request(Request.METHOD_HEAD, url);
	}

	public static Request head(String url) {
		return new Request(Request.METHOD_HEAD, new StringBuilder(url));
	}

	public static Request post(StringBuilder url) {
		return new Request(Request.METHOD_POST, url);
	}

	public static Request post(String url) {
		return new Request(Request.METHOD_POST, new StringBuilder(url));
	}

	public static Request put(StringBuilder url) {
		return new Request(Request.METHOD_PUT, url);
	}

	public static Request put(String url) {
		return new Request(Request.METHOD_PUT, new StringBuilder(url));
	}

	public static Request patch(StringBuilder url) {
		return new Request(Request.METHOD_PATCH, url);
	}

	public static Request patch(String url) {
		return new Request(Request.METHOD_PATCH, new StringBuilder(url));
	}

	public static Request delete(StringBuilder url) {
		return new Request(Request.METHOD_DELETE, url);
	}

	public static Request delete(String url) {
		return new Request(Request.METHOD_DELETE, new StringBuilder(url));
	}

	public static Request custom(String method, StringBuilder url) {
		boolean isCustomMethod = 
				   !Request.METHOD_GET.equalsIgnoreCase(method)
				&& !Request.METHOD_HEAD.equalsIgnoreCase(method)
				&& !Request.METHOD_POST.equalsIgnoreCase(method)
				&& !Request.METHOD_PUT.equalsIgnoreCase(method)
				&& !Request.METHOD_DELETE.equalsIgnoreCase(method)
				&& !Request.METHOD_PATCH.equalsIgnoreCase(method);

		return new Request(method.toUpperCase(), url, isCustomMethod);
	}

    public static Request custom(String method, StringBuilder url, boolean useApache) {
        return new Request(method.toUpperCase(), url, useApache);
    }

    public static void addGetParam(StringBuilder link, String name, String value) throws UnsupportedEncodingException {
        if (value == null)
            return;
        if (link.indexOf("?") >= 0)
            link.append("&");
        else
            link.append("?");
        link.append(URLEncoder.encode(name, "UTF-8")).append('=').append(URLEncoder.encode(value, "UTF-8"));
    }    
    
    public static void addPostParam(StringBuilder link, String name, String value) throws UnsupportedEncodingException {
        if (value == null)
            return;
        if (link.length() > 0)
            link.append("&");
        link.append(URLEncoder.encode(name, "UTF-8")).append('=').append(URLEncoder.encode(value, "UTF-8"));
    }

    public static void addCookieParam(StringBuilder cookie, String name, String value) throws UnsupportedEncodingException {
        if (value == null)
            return;
        if (cookie.length() > 0)
        	cookie.append("; ");
//        cookie.append(URLEncoder.encode(name, "UTF-8")).append('=').append(URLEncoder.encode(value, "UTF-8"));
		cookie.append(name).append('=').append(value);
    }    
}
