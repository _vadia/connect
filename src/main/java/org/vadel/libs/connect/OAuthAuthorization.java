package org.vadel.libs.connect;

public class OAuthAuthorization implements Authorization {

	final String token;
	
	public OAuthAuthorization(String token) {
		this.token = token;
	}
	
	@Override
	public void setCredentials(Request conn) {
		conn.headers.put("Authorization", "OAuth " + token);
	}

}
