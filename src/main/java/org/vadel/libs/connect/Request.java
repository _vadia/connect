package org.vadel.libs.connect;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class Request {

    static boolean apacheDefined;

    static {
        try {
            Class<?> cls = Class.forName( "org.apache.http.client.methods.HttpPost");
            apacheDefined = cls != null;
        } catch(ClassNotFoundException e) {
            apacheDefined = false;
        }               
    }

    public static final int CONNECT_TIMEOUT = 10000;
    public static final int READ_TIMEOUT   = 20000;

    public static final String METHOD_GET    = "GET";
    public static final String METHOD_HEAD   = "HEAD";
    public static final String METHOD_POST   = "POST";
    public static final String METHOD_PUT    = "PUT";
    public static final String METHOD_PATCH  = "PATCH";
    public static final String METHOD_DELETE = "DELETE";
    
    public static final String HEADER_COOKIE          = "Cookie";
    public static final String HEADER_USER_AGENT      = "User-Agent";
    public static final String HEADER_HOST            = "Host";
    public static final String HEADER_REFERER         = "Referer";
    public static final String HEADER_ACCEPT          = "Accept";
    public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";
    public static final String HEADER_CACHE_CONTROL   = "Cache-Control";
    public static final String HEADER_CONNECTION      = "Connection";
    
    public static final String ACCEPT_ALL = "*/*";
    public static final String ACCEPT_ENCODING_ZIPPED = "gzip, deflate";//, sdch";
    public static final String USER_AGENT_CHROME = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36";
    
    final String method;
    final StringBuilder url;
    final boolean apacheResponse;
    boolean isRange;
    Map<String, String> headers, cookies;
    Authorization authorization;
    Boolean followRedirect;

    InputStream   dataStream;
    StringBuilder dataString;
    Map<String, String> dataForms;

    public Request(String method, StringBuilder url) {
        this(method, url, false);
    }

    public Request(String method, StringBuilder url, boolean apacheResponse) {
        this.method = method;
        this.url = url;
        this.apacheResponse = apacheDefined && apacheResponse;
    }
    
    public String getMethod() {
        if (method == null)
            return METHOD_GET;
        else
            return method;
    }

    public Object getData() {
        if (dataStream != null)
            return dataStream;
        else if (dataString != null)
            return dataString;
        else if (dataForms != null)
            return dataForms;
        return null;
    }

    public StringBuilder dataFormsAsString() {
        if (dataForms == null)
            return dataString;
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, String> entry : dataForms.entrySet()) {
            try {
                Connect.addPostParam(str, entry.getKey(), entry.getValue());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    public Response execute() throws IOException {
        finishBuild();
        return apacheResponse
                ? new ResponseApache(this) 
                : new ResponseHttpConnection(this);
    }
    
    public Response execute(Class<Response> respClass) throws IOException, IllegalArgumentException, SecurityException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        finishBuild();
        return respClass.getConstructor(respClass).newInstance(this);
    }
    
    Map<String, String> getHeaders() {
        if (headers == null)
            headers = new HashMap<String, String>();
        return headers;
    }

    public Request rangeFrom(long from) {
        return range(from, -1);
    }

    public Request rangeTo(long to) {
        return range(-1, to);
    }

    // Range: bytes=64397516-80496894
    public Request range(long from, long to) {
        StringBuilder str = new StringBuilder("bytes=");
        if (from >= 0)
            str.append(from);
        str.append('-');
        if (to >= 0)
            str.append(to);
        getHeaders().put("Range", str.toString());
        isRange = true;
        return this;
    }

    public Request withHeader(String name, String value) {
        getHeaders().put(name, value);
        return this;
    }

    public Request withHeaderIf(String name, String value) {
        Map<String, String> m = getHeaders();
        if (!m.containsKey(name))
            m.put(name, value);
        return this;
    }

    public Request withHeaders(Map<String, String> values) {
        if (values == null)
            return this;
        getHeaders().putAll(values);
        return this;
    }

    protected Map<String, String> getCookies() {
        if (cookies == null)
            cookies = new HashMap<String, String>();
        return cookies;
    }

    public Request withCookie(String name, String value) {
        getCookies().put(name, value);
        // String s = getHeaders().get(HEADER_COOKIE);
        // StringBuilder cookie = (s == null) ? new StringBuilder() : new StringBuilder(s);
        // Connect.addCookieParam(cookie, name, value);
        // headers.put(HEADER_COOKIE, cookie.toString());
        return this;
    }

    public Request withCookies(Map<String, String> values) {
        if (values == null)
            return this;
        getCookies().putAll(values);
        // String s = getHeaders().get(HEADER_COOKIE);
        // StringBuilder cookie = (s == null) ? new StringBuilder() : new StringBuilder(s);
        // Connect.addCookieParam(cookie, name, value);
        // headers.put(HEADER_COOKIE, cookie.toString());
        return this;
    }

    void finishBuild() {
        if (cookies != null) {
            String s = getHeaders().get(HEADER_COOKIE);
            StringBuilder cookie = (s == null) ? new StringBuilder() : new StringBuilder(s);
            for (String name : cookies.keySet()) {
                try { 
                    Connect.addCookieParam(cookie, name, cookies.get(name));
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            headers.put(HEADER_COOKIE, cookie.toString());
        }
    }

    public Request withUserAgent(String value) {
        getHeaders().put(HEADER_USER_AGENT, value);
        return this;
    }

    public Request asChrome() {
        Map<String, String> headers = getHeaders();
        headers.put(HEADER_USER_AGENT, USER_AGENT_CHROME);
        headers.put(HEADER_ACCEPT, ACCEPT_ALL);
        headers.put(HEADER_ACCEPT_ENCODING, ACCEPT_ENCODING_ZIPPED);
        headers.put("DNT", "1");
        headers.put(HEADER_CACHE_CONTROL, "max-age=0");
        headers.put(HEADER_CONNECTION, "keep-alive");
        return this;
    }
    
    public Request asRefresh() {
        String link = url.toString();
        Map<String, String> headers = getHeaders();
        headers.put(HEADER_HOST,    Utils.getUrlHostName(link));
        headers.put(HEADER_REFERER, link);
        return this;
    }
    
    public Request addQuery(String name, String value) throws UnsupportedEncodingException {
        Connect.addGetParam(url, name, value);
        return this;
    }

    public Request addDataForm(String name, String value) throws UnsupportedEncodingException {
        if (dataForms == null)
            dataForms = new HashMap<String, String>();
        dataForms.put(name, value);
        return this;
    }

    public Request addDataForms(String data) throws UnsupportedEncodingException {
        if (dataForms == null)
            dataForms = new HashMap<String, String>();
        String[] args = data.split("&");
        for (int i = 0; i < args.length; i++) {
            String[] vals = args[i].split("=");
            if (vals.length >= 2)
                dataForms.put(vals[0], URLDecoder.decode( vals[1], "UTF-8") );
            else
                dataForms.put(vals[0], "");
        }
        return this;
    }

    public Request withData(StringBuilder data) {
        this.dataString = data;
        return this;
    }

    public Request withData(InputStream data) {
        this.dataStream = data;
        return this;
    }
    
    boolean isFollowRedirect() {
    	return followRedirect != null && followRedirect;    	
    }
    
    public Request withFollowRedirect(boolean value) {
        followRedirect = value;
        return this;
    }
    
    public Request withAuthorization(Authorization auth) {
        this.authorization = auth;
        return this;
    }
}
