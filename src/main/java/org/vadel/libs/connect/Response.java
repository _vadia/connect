package org.vadel.libs.connect;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.List;

@SuppressWarnings("unused")
public abstract class Response {

	static final String LAST_MODIFIED  = "Last-Modified";
	static final String SERVER         = "Server";
	static final String AGE            = "Age";
	static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	static final String ACCEPT_RANGES  = "Accept-Ranges";
	static final String SET_COOKIE     = "Set-Cookie";
    static final String EXPIRES        = "Expires";
    static final String DATE           = "Date";
    static final String CONTENT_LENGTH = "Content-Length";
    static final String CONTENT_TYPE   = "Content-Type";

    final boolean rangeRequest;
	InputStream in;

    protected Response(boolean rangeRequest) {
        this.rangeRequest = rangeRequest;
    }

	abstract Response execute() throws IOException;
	
	public abstract StringBuilder getSetCookies();

    public abstract List<String> getSetCookiesList();

    public String getSetCookiesAsString() {
		StringBuilder str = getSetCookies();
		if (str == null)
			return null;
		return str.toString();	
	}

	public abstract String getSetCookie(String name);

//	public abstract StringBuilder getHeader(String name);

    public ContentRangeStruct getContentRange() {
        String s = getHeaderFirst("Content-Range");
        if (s == null)
            return null;
        int i0 = s.indexOf(' ') + 1;
        int i1 = s.lastIndexOf('/');
        if (i1 < 0)
            return null;
        ContentRangeStruct res = new ContentRangeStruct();
        res.length = Utils.getLongDef(s.substring( i1 + 1), -1);
        String range = s.substring(i0, i1);
        i1 = range.indexOf('-');
        res.start = Utils.getLongDef(range.substring(0, i1), -1);
        res.end   = Utils.getLongDef(range.substring(i1 + 1), -1);
        return res;
    }

	public abstract long getLastModified();

	public abstract long getExpires();

	public abstract long getDate();

	public abstract int getContentLength();

	public abstract String getContentType();

	public String getAcceptRanges() {
		return getHeaderFirst(ACCEPT_RANGES);
	}

	public String getAccessControlAllowOrigin() {
		return getHeaderFirst(ACCESS_CONTROL_ALLOW_ORIGIN);
	}

	public String getAge() {
		return getHeaderFirst(AGE);
	}

	public String getServer() {
		return getHeaderFirst(SERVER);
	}
	
	public abstract int getCode() throws IOException;

	public abstract StringBuilder getHeader(String name);

//    public abstract List<String> getHeaders(String name);

	public abstract String getHeaderFirst(String name);
	
	public boolean isOk() throws IOException {
        int code = getCode();
		return code >= 200 && code < 300;
	}

    public boolean isPartialContent() throws IOException {
        return getCode() == 206;
    }

    public boolean isRangeContent() throws IOException {
        boolean isPartial   = isPartialContent();
        String acceptRange  = getHeaderFirst("Accept-Ranges");
        String contentRange = getHeaderFirst("Content-Range");
        return isPartial
                || Utils.isNotEmpty(acceptRange)
                || Utils.isNotEmpty(contentRange);
    }

    public boolean isRederect() throws IOException {
        int code = getCode();
        return code >= 300 && code < 400;
    }

	public InputStream getInputEncoding() throws IOException {
		execute();
		return in;
	}

    public StringBuilder asString() throws IOException {
        return asString("UTF-8");
    }

	public StringBuilder asString(String charset) throws IOException {
		StringBuilder str = new StringBuilder();
		if (in != null) {
			int n;
			char[] buff = new char[512];
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset));
			while((n = reader.read(buff)) >= 0) {
				str.append(buff, 0, n);
			}
			reader.close();
		}
		return str;
	}

	public StringBuilder asStringIfOk() throws IOException {
		if (isOk())
			return asString();
		else
			return null;
	}

    public StringBuilder asStringIfOk(String charset) throws IOException {
        if (isOk())
            return asString(charset);
        else
            return null;
    }

	public JSONObject asJson() throws JSONException, IOException {
		return new JSONObject(asString().toString());
	}

	public JSONObject asJsonIfOk() throws JSONException, IOException {
		if (isOk())
			return asJson();
		else
			return null;
	}

    public long save(String file, boolean overwrite) {
        return save(file, overwrite, null);
    }

    public long save(String file, boolean overwrite, OnDownloadListener listener) {
        File f = new File(file);
        if (f.exists() && !overwrite)
            return -1;
        return save(f, true, listener);
    }

    public long saveAuto(File f) {
//        if (f.exists() && rangeRequest) {
//
//        }
        try {
            return save(new FileOutputStream(f, f.exists() && rangeRequest), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long save(File f, boolean overwrite) {
        return save(f, overwrite, null);
    }

    public long save(File f, boolean overwrite, OnDownloadListener listener) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(f, !overwrite);
            return save(out, listener);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }

    public long save(OutputStream out, final OnDownloadListener listener) {
        try {
            InputStream in = getInputEncoding();
            if (in == null)
                return -1;

            Utils.OnBatchCopyListener copyListener;
            if (listener != null) {
                ContentRangeStruct range = getContentRange();
                final long start = (range != null) ? Math.max(range.start, 0) : 0;
                final long length = start + getContentLength();
                copyListener = new Utils.OnBatchCopyListener() {
                    @Override
                    public void onCopy(long l) {
                        listener.onDownload(start + l, length);
                    }
                };
            } else {
                copyListener = null;
            }

            long result = Utils.copy(in, out, copyListener);
            out.flush();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Utils.closeQuietly(out);
        }
        return -1;
    }

    public static class ContentRangeStruct {
        public long start;
        public long end;
        public long length;

        @Override
        public String toString() {
            StringBuilder str = new StringBuilder("bytes ");
            if (start >= 0)
                str.append(start);
            str.append("-");
            if (end >= 0)
                str.append(end);
            str.append("/");
            if (length >= 0)
                str.append(length);
            return str.toString();
        }
    }

    public interface OnDownloadListener {
        void onDownload(long bytes, long length);
    }
}
