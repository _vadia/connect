package org.vadel.libs.connect;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

public class ResponseApache extends Response {

    static RequestConfig requestConfig = RequestConfig.custom()
//            .setSocketTimeout(TIMEOUT_MILLIS)
              .setConnectTimeout(Request.CONNECT_TIMEOUT)
              .setConnectionRequestTimeout(Request.READ_TIMEOUT)
              .build();
    
    HttpClient client;
    HttpResponse resp;

    Map<String, CookieItem> cookies;

    WebRequest webRequest;
    static final SimpleDateFormat dateFormater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
    
    public ResponseApache(Request request) throws IOException {
        super(request.isRange);
        webRequest = new WebRequest(request.url.toString(), request.getMethod());

        // authorization
        if (request.authorization != null)
            request.authorization.setCredentials(request);
        // headers
        if (request.headers != null)
            for (Entry<String, String> entry : request.headers.entrySet()) {
                webRequest.addHeader(entry.getKey(), entry.getValue());
            }
        
        // ReadTimeout & ConnectTimeout
        webRequest.setConfig(requestConfig); 
        // data
        if (request.getData() != null) {
            if (request.dataStream != null) {
                webRequest.setEntity(new InputStreamEntity(request.dataStream));
            } else if (request.dataString != null) {
                webRequest.setEntity(new StringEntity(request.dataString.toString()));
            } else if (request.dataForms != null) {
                List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

                for (Entry<String, String> entry : request.dataForms.entrySet()) {
                    urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                webRequest.setEntity(new UrlEncodedFormEntity(urlParameters));
            }
        }
        // followRedirect
        if (request.followRedirect != null) {
            if (request.followRedirect)
                client = HttpClientBuilder.create()
                        .setRedirectStrategy(new LaxRedirectStrategy())
                        .build();
            else 
                client = HttpClientBuilder.create()
                        .disableRedirectHandling()
                        .build();           
        } else {
            client = HttpClientBuilder.create()
                    .build();           
        }
        execute();
    }
    
    boolean executed;

    Response execute() throws IOException {
        if (executed)
            return this;
        resp = client.execute(webRequest);
        in = resp.getEntity().getContent();
        executed = true;
        return this;
    }

    void initCookiesIfNeed() {
        if (cookies != null)
            return;
        cookies = new HashMap<String, CookieItem>();
        Header[] headers = resp.getHeaders(SET_COOKIE);
        if (headers == null)
            return;

        for (int i = 0; i < headers.length; i++) {
            Header hdr = headers[i];
            CookieItem cookie = CookieItem.parse(hdr.getValue());
            if (cookie != null)
                cookies.put(cookie.name.toLowerCase(), cookie);
        }
    }
        
    @Override
    public StringBuilder getSetCookies() {
        return getHeader(SET_COOKIE);
    }

    @Override
    public List<String> getSetCookiesList() {
        Header[] headers = resp.getHeaders(SET_COOKIE);
        if (headers == null)
            return null;
        List<String> res = new ArrayList<String>();
        for (int i = 0; i < headers.length; i ++) {
            Header h = headers[i];
            res.add( h.getName() + "=" + h.getValue() );
        }
        return res;
    }

    @Override
    public String getSetCookie(String name) {
        initCookiesIfNeed();
        CookieItem result = cookies.get(name.toLowerCase());
        return result != null ? result.value : null;
    }

    @Override
    public long getLastModified() {
        return getHeaderDate(LAST_MODIFIED);
    }

    @Override
    public long getExpires() {
        return getHeaderDate(EXPIRES);
    }

    @Override
    public long getDate() {
        return getHeaderDate(DATE);
    }

    @Override
    public int getContentLength() {
        String s = getHeaderFirst(CONTENT_LENGTH);
        if (s == null)
            return 0;
        return Utils.getIntDef(s, 0);
    }

    @Override
    public String getContentType() {
        return getHeaderFirst(CONTENT_TYPE);
    }

    @Override
    public int getCode() throws IOException {
        return resp.getStatusLine().getStatusCode();
    }

//    @Override
//    public List<String> getHeaders(String name) {
//    }

    @Override
    public StringBuilder getHeader(String name) {
        Header[] headers = resp.getHeaders(name);
        if (headers == null)
            return null;

        StringBuilder res = new StringBuilder();
        for (int i = 0; i < headers.length; i++) {
            if (res.length() > 0)
                res.append("; ");
            res.append(headers[i].getValue());
        }
        return res;     
    }
    
    public String getHeaderFirst(String name) {
        Header header = resp.getFirstHeader(name);
        if (header == null)
            return null;
        return header.getValue();
    }
    
    public long getHeaderDate(String name) {
        String s = getHeaderFirst(name);
        if (s == null)
            return 0l;
        Date dt;
        try {
            dt = dateFormater.parse(s);
            return dt.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public static class CookieItem {

        public final String name;
        public final String value;
        public Map<String, String> params;

        public static CookieItem parse(String s) {
            return parse(s, false);
        }

        public static CookieItem parse(String s, boolean withAllParams) {
            String[] args = s.split(";\\s+");
            if (args == null || args.length == 0)
                return null;
            String[] vals = args[0].split("=");
            CookieItem cookie;
            if (vals.length >= 2)
                cookie = new CookieItem(vals[0], vals[1]);
            else
                cookie = new CookieItem(vals[0], null);
            if (withAllParams) {
                cookie.params = new HashMap<String, String>();
                for (int i = 1; i < args.length; i++) {
                    if (vals.length >= 2)
                        cookie.params.put(vals[0], vals[1]);
                    else
                        cookie.params.put(vals[0], null);
                }
            }
            return cookie;
        }

        private CookieItem(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }
}
