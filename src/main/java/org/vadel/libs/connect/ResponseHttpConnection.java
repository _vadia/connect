package org.vadel.libs.connect;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ResponseHttpConnection extends Response {

	static {

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
        	
        		@Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
        		
        		@Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
        		
        		@Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
 
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        
		// Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
        };
 
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);		
	}
	
	public final HttpURLConnection conn;
	Map<String, List<String>> headers;
	
	public ResponseHttpConnection(Request request) throws IOException {
		super(request.isRange);
		HttpURLConnection conn2 = connect(request.url.toString(), request);
		if (conn2 != null && request.isFollowRedirect()) {
			int code = conn2.getResponseCode();
			if (code >= 300 && code < 400) {
				String newUrl = conn2.getHeaderField("Location");
				if (newUrl != null && newUrl.length() > 0)
					conn2 = connect(newUrl, request);
			}
			
		}
		conn = conn2;
//        return new Response2(conn);
        execute();
	}
	
	private static HttpURLConnection connect(String uri, Request request) throws IOException {
		URL url = new URL(uri);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (request.followRedirect != null)
			conn.setInstanceFollowRedirects(request.followRedirect);
        if (request.method != null) {
            String method = request.method.toUpperCase();

            if (Request.METHOD_GET.equals(method) ||
                Request.METHOD_HEAD.equals(method) ||
                Request.METHOD_POST.equals(method) ||
                Request.METHOD_PUT.equals(method) ||
                Request.METHOD_PATCH.equals(method) ||
                Request.METHOD_DELETE.equals(method)) {
                conn.setRequestMethod(request.method);
            } else {
                conn.setRequestProperty("X-HTTP-Method-Override", request.method);
                conn.setRequestMethod(Request.METHOD_POST);
            }
		}
        
        if (request.authorization != null)
        	request.authorization.setCredentials(request);

        setRequestHeaders(conn, request.headers);
        conn.setReadTimeout(Request.READ_TIMEOUT);
        conn.setConnectTimeout(Request.CONNECT_TIMEOUT);
        if (request.getData() != null) {
            conn.setDoOutput(true);
            if (request.dataStream != null) {
                Utils.copy(request.dataStream, conn.getOutputStream());
            } else if (request.dataString != null) {
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(request.dataString.toString());
                wr.flush();
            } else if (request.dataForms != null) {
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(request.dataFormsAsString().toString());
                wr.flush();
            }
        }
        return conn;
	}
	
	boolean executed;

	Response execute() throws IOException {
		if (executed)
			return this;
//		if (isOk()) {
		in = Utils.getInputEncoding(conn, !isOk());
//		} else {
//            in = conn.getErrorStream();
//        }
        executed = true;
        return this;
	}

	@Override
	public StringBuilder getSetCookies() {
		return getHeader(SET_COOKIE);		
	}

	@Override
	public List<String> getSetCookiesList() {
		if (headers == null)
			headers = conn.getHeaderFields();
		if (headers == null)
			return null;
		return headers.get(SET_COOKIE);
	}

	@Override
	public String getSetCookie(String name) {
		if (headers == null)
			headers = conn.getHeaderFields();
		if (headers == null)
			return null;
		List<String> lst = headers.get(SET_COOKIE);
		if (lst == null)
			return null;
		for (int i = 0; i < lst.size(); i++) {
			String[] s = lst.get(i).split(";");
			String[] values = s[0].split("=");
			if (values.length == 0)
				continue;
			
			if (values[0].equals(name))
				return values[1];
		}
		return null;		
	}

	@Override
	public StringBuilder getHeader(String name) {
		if (headers == null)
			headers = conn.getHeaderFields();
		if (headers == null)
			return null;
		List<String> lst = headers.get(name);
		if (lst == null)
			return null;
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < lst.size(); i++) {
			if (res.length() > 0)
				res.append("; ");
			res.append(lst.get(i));
		}
		return res;
	}

//	public StringBuilder getHeaderByIndex(int i) {
//		if (headers == null)
//			headers = conn.getHeaderFields();
//		if (headers == null)
//			return null;
//
//		List<String> lst = headers.get(headers.keySet);
//		if (lst == null)
//			return null;
//		StringBuilder res = new StringBuilder();
//		for (int i = 0; i < lst.size(); i++) {
//			if (res.length() > 0)
//				res.append("; ");
//			res.append(lst.get(i));
//		}
//		return res;
//	}

//	public int getHeadersCount() {
//		if (headers == null)
//			headers = conn.getHeaderFields();
//		if (headers == null)
//			return 0;
//		return headers.size();
//	}
	
	public long getLastModified() {
		return conn.getLastModified();
	}
	
	public long getExpires() {
		return conn.getExpiration();
	}

	public long getDate() {
		return conn.getDate();
	}
	
	public int getContentLength() {
		return conn.getContentLength();
	}
	
	public String getContentType() {
		return conn.getContentType();
	}
		
	public int getCode() throws IOException {
		return conn.getResponseCode();		
	}

	@Override
	public String getHeaderFirst(String name) {
		return conn.getHeaderField(name);
	}

    static void setRequestHeaders(HttpURLConnection conn, Map<String, String> headers) {
//      conn.setRequestProperty("User-Agent", GlobalLinksUtils.CHROME_USER_AGENT);
    	conn.setRequestProperty(Request.HEADER_ACCEPT_ENCODING, Request.ACCEPT_ENCODING_ZIPPED);
        if (headers == null) 
        	return;
    	for (String key : headers.keySet()) {
        	conn.setRequestProperty(key, headers.get(key));
    	}
    }

}
