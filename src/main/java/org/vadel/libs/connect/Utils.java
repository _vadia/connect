package org.vadel.libs.connect;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Created by Babin on 29.03.2016.
 */
public class Utils {

    public static int getIntDef(String value, int def) {
        if (value == null)
            return def;
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    public static long getLongDef(String value, long def) {
        if (value == null)
            return def;
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    public static boolean isNotEmpty(String value) {
        return value != null && value.trim().length() > 0;
    }

    /**
     * Возвращает название сайта
     * @param url - проверяемый URL
     * @return название сайта, или null если url не валидный
     */
    public static String getUrlHostName(String url) {
        int i1 = url.indexOf("://");
        if (i1 < 0)
            return null;
        i1 += 3;
        int i2 = url.indexOf("/", i1);
        if (i2 < 0)
            return url.substring(i1);
        return url.substring(i1, i2);
    }

    public static InputStream getInputEncoding(HttpURLConnection connection, boolean fromError) throws IOException {
        InputStream in;
        String encoding = connection.getContentEncoding();
        if (fromError) {
            in = connection.getErrorStream();
        } else {
            in = connection.getInputStream();
        }
        if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
            in = new GZIPInputStream(in);//connection.getInputStream());
        } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
            in = new InflaterInputStream(in);//connection.getInputStream(), new Inflater(true));
//        } else {
//            in = connection.getInputStream();
        }
        return in;
    }

    /**
     * Buffer size when reading from input stream.
     * @since ostermillerutils 1.00.00
     */
    private static int BUFFER_SIZE = 1024;

    public static int BATCH_SIZE = 100*BUFFER_SIZE;

    /**
     * Copy the data from the input stream to the output stream.
     * @param in data source
     * @param out data destination
     * @throws IOException in an input or output error occurs
     * @since ostermillerutils 1.00.00
     */
    public static long copy(InputStream in, OutputStream out) throws IOException {
        long result = 0;
        byte[] buffer = new byte[BUFFER_SIZE];
        int read;
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            result += read;
        }
        return result;
    }

    /**
     * Copy the data from the input stream to the output stream.
     * @param in data source
     * @param out data destination
     * @throws IOException in an input or output error occurs
     * @since ostermillerutils 1.00.00
     */
    public static long copy(InputStream in, OutputStream out, OnBatchCopyListener listener) throws IOException {
        if (listener == null)
            return copy(in, out);

        long result = 0;
        long next = BATCH_SIZE;
        byte[] buffer = new byte[BUFFER_SIZE];
        int read;
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            result += read;
            if (result >= next) {
                listener.onCopy(result);
                next += BATCH_SIZE;
            }
        }
        return result;
    }

    public static void closeQuietly(Closeable in) {
        if (in != null)
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public interface OnBatchCopyListener {
        void onCopy(long n);
    }
}
