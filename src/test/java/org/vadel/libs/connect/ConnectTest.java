package org.vadel.libs.connect;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class ConnectTest {

//	@Test
//	public void testGetOk() throws IOException {
//		assertTrue(
//			Connect.get("http://stackoverflow.com/questions/10744715/howto-base64-encode-an-java-object-using-org-apache-commons-codec-binary-base64")
//				.asChrome()
//				.execute()
//				.isOk()
//				);
//	}

	@Test
	public void testGetError() throws IOException {
		assertFalse(
			Connect.get("http://stackoverflow.com/questions/10744715/howto-base64-encode-an-java-object-using-org-apache-commons-codec-binary-base641")
				.withFollowRedirect(false)
				.asChrome()
				.execute()
				.isOk()
				);
	}

//	@Test
//	public void testCustomOk() throws IOException {
//		assertTrue(
//			Connect.custom("get", new StringBuilder("http://stackoverflow.com/questions/10744715/howto-base64-encode-an-java-object-using-org-apache-commons-codec-binary-base64"))
//				.asChrome()
//				.execute()
//				.isOk()
//				);
//	}

	@Test
	public void testCustomError() throws IOException {
		assertFalse(
			Connect.custom("get", new StringBuilder("http://stackoverflow.com/questions/10744715/howto-base64-encode-an-java-object-using-org-apache-commons-codec-binary-base641"))
				.withFollowRedirect(false)
				.asChrome()
				.execute()
				.isOk()
				);
	}
	
//	@Test
//	public void testGetAsChrome() throws IOException {
//		StringBuilder html = Connect.get("http://adultmanga.ru/love_stage")
//                .asChrome()
//                .execute()
//                .asStringIfOk();
//		assertNotNull(html);
//		assertTrue(html.length() > 10);
//	}

//	@Test
//	public void testDownload() throws IOException {
//        File f = new File("1.jpg");
//        long res = Connect.get("http://a.adultmanga.ru/uploads/pics/00/04/008.jpg")
//                .execute()
//                .save(f, true);
//        assertTrue( res > 0 );
//        assertTrue( f.delete() );
//	}
	
	@Test
	public void testHttpToHttpsRedirect() throws IOException {
//        File f = new File("1.jpg");
		assertTrue( Connect.get("http://www.radio-t.com/images/cover.jpg")
        		.withFollowRedirect(true)
                .execute()
                .isOk() );
	}

	final int rangeFrom     = 84089870;
	final int rangeTo       = 84129874;
	final int contentLength = 84129874;

	@Test
    public void testDownloadRange() throws IOException {
        Utils.BATCH_SIZE = 1024*10;
        File f = new File("1.jpg");
        Response response = Connect.get("http://nn.radio-t.com/rtfiles/rt_podcast466.mp3")
                .range(rangeFrom, rangeTo)
                .execute();
        assertTrue(response.isRangeContent());
        long res = response.save(f, true, new Response.OnDownloadListener() {
            @Override
            public void onDownload(long bytes, long length) {
                System.out.println("download progress:" + bytes + "/" + length);
            }
        });
        assertEquals( res,  rangeTo - rangeFrom);
        assertTrue( res > 0 );
        assertTrue( f.delete() );
    }

    @Test
    public void testHeadRange() throws IOException {
        Response response = Connect.head("http://nn.radio-t.com/rtfiles/rt_podcast466.mp3")
                .range(rangeFrom, rangeTo)
                .execute();
        assertTrue(response.isRangeContent());

        Response.ContentRangeStruct range = response.getContentRange();
        assertEquals( range.start,  rangeFrom );
        assertEquals( range.end,    rangeTo - 1 );
        assertEquals( range.length, contentLength );
        assertEquals( rangeTo - rangeFrom, response.getContentLength() );
    }

//	@Test
//	public void testPostStringBuilder() {
//		fail("Not yet implemented");
//	}

//	@Test
//	public void testPutStringBuilder() {
//		fail("Not yet implemented");
//	}

//	@Test
//	public void testDeleteStringBuilder() {
//		fail("Not yet implemented");
//	}
}
